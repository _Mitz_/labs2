using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform thePlayer;
    Vector3 offset;

    private void Start()
    {
        offset = transform.position - thePlayer.position;
    }

    private void Update()
    {
        Vector3 targetPosition = thePlayer.position + offset;
        transform.position = targetPosition;
    }
}
