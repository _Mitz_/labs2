using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float vertical;
    public float horizontal;
    public float verticalRaw;
    public float horizontalRaw; 

    public Rigidbody playerRigid;

    Vector3 targetRotation;
    public float rotationSpeed = 10f;
    public float moveSpeed = 1000f;

    void FixedUpdate()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        horizontalRaw = Input.GetAxisRaw("Horizontal");
        verticalRaw = Input.GetAxisRaw("Vertical");

        Vector3 input = new Vector3(horizontal, 0, vertical);
        Vector3 inputRaw = new Vector3(horizontalRaw, 0, verticalRaw);

        if(input.sqrMagnitude > 1f)
        {
            input.Normalize();
        }

        if(inputRaw.sqrMagnitude > 1f)
        {
            inputRaw.Normalize();
        }

        if(inputRaw != Vector3.zero)
        {
            targetRotation = Quaternion.LookRotation(input).eulerAngles;
        }

        playerRigid.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(targetRotation.x, Mathf.Round(targetRotation.y / 45) * 45, targetRotation.z), Time.deltaTime * rotationSpeed);

        Vector3 vel = input * moveSpeed * Time.deltaTime;
        playerRigid.velocity = vel;
    }
}
