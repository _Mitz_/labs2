using UnityEngine;

public class SpriteDirectionalController : MonoBehaviour
{
    //[SerializeField] float backAngle = 65f;
    //[SerializeField] float sideAngle = 155f;
    [SerializeField] Transform mainTransform;
    //[SerializeField] SpriteRenderer spriteRenderer;

    private void LateUpdate()
    {
        Vector3 camForward = new Vector3(Camera.main.transform.forward.x, 0f, Camera.main.transform.forward.z);
        Debug.DrawRay(Camera.main.transform.position, camForward * 5f, Color.magenta);

        float signedAngle = Vector3.SignedAngle(mainTransform.forward, camForward, Vector3.up);

        float angle = Mathf.Abs(signedAngle);

        /*if (angle < backAngle)
        {

        }*/
    }
}
